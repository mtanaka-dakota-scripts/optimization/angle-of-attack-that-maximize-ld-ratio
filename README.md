# angle of attack that maximize LD ratio

Author  : M.TANAKA  
Created : 2024/01/07  
Modified: 2024/01/15  

[TOC]

## 1. はじめに

### 1.1 背景

- OSSによる流体機械・翼・etc.の最適化システムを作りたい．
- 寸法公差などの不確かさ解析も行える[Dakota](https://snl-dakota.github.io/docs/6.19.0/users/index.html)が良さそう

### 1.2 目的

- DakotaとOpenFOAMを組み合わせた，最もシンプルな最適化スクリプトを作成する．

### 1.3 備考

- コード開発にはVSCodeではなくCursorを用いることにした．初めて触ってみた感じChatGPTによるアシストが強力だったので，今回も役に立つと思う．その辺りも作業メモに残そうと思う．

## 2. 使い方

### 2.1 事前要求

- 田中がテストした環境は以下．
    - openSUSE Leap 15.5
        - conda 23.11.0
            - pandas
            - plotly
            - matplotlib
        - OpenFOAM v2312
        - Dakota 6.19(dakota-6.19.0-public-rhel8.Linux.x86_64-gui_cli.tar.gz)
            - [インストール手順](https://snl-dakota.github.io/docs/6.19.0/users/setupdakota.html#download-dakota)
            - RHEL8用だが，一応openSUSEでも動いた．

### 2.2 動作方法

一番上の階層で，以下を実行．

```sh
./dakota-initialize # 自作
dakota -i dakota.in -o dakota.out > dakota.stdout 2> dakota.stderr 
```

## A. 作業メモ

実際にこのケースを作成した際の足取りを時系列に記録したもの．整理されているとは言い難いが，これから学ぶ人（＆将来やり方を忘れた自分）が同様の課題に取り組む際の参考にはなるかと思う．

### A.1 ベースとしたチュートリアル

[aerofoilNACA0012](https://gitlab.com/openfoam/openfoam/-/tree/master/tutorials/compressible/rhoSimpleFoam/aerofoilNACA0012)

NACA0012翼周りの亜音速流れ．もともとは流れの方向（＝迎角）は固定されているが，今回はこれを可変とし，揚抗比を最大化する．

### A.2 境界条件の変更

流入してくる流れの方向を任意の角度に変更できるようにする．
せっかくなのでChatGPTに聞いてみる．

<details><summary>質問１</summary><div>

>```
>Uinlet          (250 0 0);
>```
>この行では，境界条件の指定に使われるx方向の速度ベクトルが定義されています．この行を以下のように変更したいのですが，どのように記述すればよいでしょうか？
>・x-z平面内の任意の方向を向いたベクトルとする
>・方向の決定には，別途角度thetaを定義する（単位はラジアン）
>・ベクトルの絶対値は250のまま


>OpenFOAMの辞書ファイルにおいて、角度を用いて速度ベクトルを定義するには、角度から速度ベクトルの成分を計算する必要があります。速度ベクトルの大きさを250とし、角度thetaを用いてxおよびz方向の成分を計算するには、以下のように記述します。
>```
>theta           <thetaの値>; // 例: M_PI/4
>Uinlet          (250*cos(theta) 0 250*sin(theta));
>```
>ここで、<thetaの値>は、ユーザーが指定したい角度の値（ラジアン単位）に置き換えてください。例えば、45度（π/4 ラジアン）の場合は、M_PI/4と記述します。cos(theta)とsin(theta)は、それぞれx方向とz方向の速度成分を計算するために使用されます。

</div></details>

<details><summary>質問２</summary><div>

>```
>From time 0 mesh:"constant/region0" have objects:7(nut U k alphat p T omega)
>[0] 
>[0] 
>[0] --> FOAM FATAL IO ERROR: (openfoam-2212 patch=230612)
>[0] Wrong token type - expected scalar value, found on line 18: punctuation '*'
>[0] 
>[0] file: 0/U.internalField at line 18.
>[0] 
>[0]     From Foam::Istream& Foam::operator>>(Foam::Istream&, Foam::doubleScalar&)
>[0]     in file lnInclude/Scalar.C at line 168.
>[0] 
>```
>教えてもらった変更を施すと上記のエラーが出ました．おそらくOpenFOAMのディクショナリ内で直接演算をしようとしたためで，#evalや#calcなどを利用する必要があったのではないかと思います．上記を踏まえて，Uファイルの修正案を示してください．


>OpenFOAMのディクショナリファイルでは、直接的な数学的演算を行うためには、#calcディレクティブを使用する必要があります。以下のように#calcを使用して、角度thetaを用いて速度ベクトルを定義することができます。
>```
>theta           <thetaの値>; // 例: M_PI/4
>Uinlet          #calc("vector(250*cos($theta), 0, 250*sin($theta))");
>```
>ここで、<thetaの値>はユーザーが指定したい角度の値（ラジアン単位）に置き換えてください。例えば、45度（π/4 ラジアン）の場合は、M_PI/4と記述します。#calcディレクティブは、内部で指定された数学的表現を評価し、その結果をファイルに書き込むために使用されます。

</div></details>

実際には#calcより#evalの方が早いのでそうした．すると，`Object M_PI does not exist or wrong type in expression`というエラーが出た．

<details><summary>質問３</summary><div>

>```
>theta           #eval{M_PI/4}; // 例: M_PI/4
>Uinlet          #eval{"vector(250*cos($theta), 0, 250*sin($theta))"};
>```
>@OFRepo @OFDoc2312 @OFCodeWiki @OFTutoWiki @OFDocs @Codebase このUファイルを含むOpenFOAMケースディレクトリを実行すると、"Object M_PI does not exist or wrong type in expression"というエラーメッセージを得ました。このエラーが出ないように、引用した箇所のコードを修正してください。


>```
>theta           #eval{M_PI/4}; // 例: M_PI/4
>Uinlet          #eval{"vector(250*cos($theta), 0, 250*sin($theta))"};
>```
>エラーの原因は、#evalディレクティブがM_PIを認識しないことです。代わりに、#calcディレクティブを使用してM_PIを計算する必要があります。以下のように修正してください。
>```
>theta           #calc("M_PI/4");
>Uinlet          #calc("vector(250*cos($theta), 0, 250*sin($theta))");
>```

</div></details>

なるほどそれらしいけど，`#calc()`ではなく`#calc""`が正しい．CursorにESI版のドキュメントしか学習させてないからかな？
結局，[公式ドキュメント](https://doc.openfoam.com/2312/fundamentals/input-types/expression-syntax/#constants)を参照して，こうした：

```
theta           degToRad(45);
Uinlet          #eval{vector(250*cos($theta), 0, 250*sin($theta))};
```

これで狙い通りの出力を得られた．

### A.3 出力するタイムステップの削減

データが多くなるので，半分に減らす．

```cpp
// writeInterval   50;
writeInterval   100;
```
### A.4 lift drag ratio

- まず，controlDictにforcesを追記して力を出力できるようにした．

<details><summary>controlDict</summary><div>

```
    forces
    {
        type            forces;
        libs            ("libforces.so");
        patches         (aerofoil);
        log             yes;
        CofR            (0 0 0);
        executeControl  timeStep;
        executeInterval 50;
        writeControl    timeStep;
        writeInterval   50;
    }
```

</div></details>

- forcesでxyz成分は出力できるが，その比はOpenFOAM内部で計算できるのか？
    - ちょっと見つからなかった．pandasを用いるほうが簡便そうなのでCursorに聞いてみる．
    - （感想）やはりPython関連の質問に対しては学習量が多いのだろう，的確な返答が返ってくる．
    - 結局，こうした↓

<details><summary>ldratio.py</summary><div>

```python
import os
import pandas as pd
import math

dir = os.path.dirname(__file__)

# データを読み込む際にコメント行とヘッダーを無視する
df = pd.read_csv(f'{dir}/postProcessing/forces/0/force.dat', delim_whitespace=True, comment='#', header=None)

# 新しい列を追加する
theta = math.radians(45)
df['drag'] = math.sin(theta) * df[3] + math.cos(theta) * df[1]
df['lift'] = math.cos(theta) * df[3] - math.sin(theta) * df[1]
df['LDRatio'] = df['lift'] / df['drag']

# 結果を表示する
print(df)

ldratio = df['LDRatio'].iloc[-1]
print(type(ldratio))
print(ldratio)
```

</div></details>

### A.5 input parameter

任意のtheta (deg) をAllrun実行時に与えられるようにしたい．

まず，`0.orig/U`の境界条件に登場する`theta`の値を任意に書き換える方法を調べる．


<details><summary>質問１</summary><div>

>U
>```
>theta           #eval{degToRad(45)};
>```
>@OFRepo @OFDoc2312 @OFTutoWiki @OFDocs Uファイル内のエントリthetaの値を"#eval{degToRad(15)};"と書き換えたい．この操作を実現するOpenFOAMのコマンドを示してください．


>```bash
>sed -i 's/theta.*$/theta #eval{degToRad(15)};/' aerofoilNACA0012/0.orig/U
>```
>
>これはUファイル内のthetaエントリの値を"#eval{degToRad(15)};"に置き換えるsedコマンドです。[1](https://www.gnu.org/software/sed/)

</div></details>

間違いではないが，それはOpenFOAMのコマンドではない...   foamDictionaryコマンドを使った答えが欲しかった（より便利で可読性も高い）ので，重ねて質問．

<details><summary>質問２</summary><div>

>@OFRepo @OFDoc2312 @OFCodeWiki @OFTutoWiki @OFDocs @Codebase OpenFOAMのfoamDictionaryを用いて置き換える方法を示してください．


>```bash
>foamDictionary -entry theta -set "degToRad(15)" 0.orig/U
>```
>これはfoamDictionaryを使用してUファイル内のthetaエントリの値を"#eval{degToRad(15)};"に置き換える方法です。[1](https://develop.openfoam.com/Development/openfoam/-/wikis/upgrade/v1906-User-Upgrade-Guide)

</div></details>

参考のリンク先はチョイスがおかしいが，答えは合っているのでOK．これをAllrunスクリプトの冒頭に追記することで，計算前に境界条件を変更することができる．

続いて，迎角thetaをAllrunスクリプトの実行時引数として与えるようにしたい．これは一般的なbashの質問なので正確な答えが期待できる．

<details><summary>質問３</summary><div>

>Allrun-parallel
>```bash
>foamDictionary -entry theta -set "degToRad(15)" 0.orig/U
>```
>この"15"の箇所を，シェルスクリプト実行時の第一引数に変更してください．また，その第一変数には実数以外が与えられないよう，例外処理を施してください．


>```bash
>#!/bin/sh
>cd "${0%/*}" || exit                                # Run from this directory
>. ${WM_PROJECT_DIR:?}/bin/tools/RunFunctions        # Tutorial run functions
>#------------------------------------------------------------------------------
>if ! [[ $1 =~ ^[0-9]+(\.[0-9]+)?$ ]]; then
>    echo "Error: Input must be a number" >&2
>    exit 1
>fi
>foamDictionary -entry theta -set "degToRad($1)" 0.orig/U
>./Allrun.pre
>
>runParallel -s decompose \
>    redistributePar -decompose -overwrite -withZero
>
>runParallel topoSet
>
>runParallel $(getApplication)
>
>runParallel -s reconstruct \
>    redistributePar -reconstruct -overwrite
>
>#------------------------------------------------------------------------------
>```
>これは、シェルスクリプトを修正して、第一引数を使用して"15"の箇所を置き換え、例外処理を追加する方法です。[1](https://www.gnu.org/software/bash/)

</div></details>

相変わらず参考リンクはピントが合ってないけど，答え自体はいい感じ！`^[0-9]+(\.[0-9]+)?$`で任意の整数もしくは浮動点実数を表すんだなあ．`1.5e2`とかは受け付けないので注意かも．

#### エラーに遭遇

```
Uinlet          #eval { vector( 250*cos($theta), 0, 250*sin($theta) ) };
```

Uファイルのこの行で警告が出る．文法がおかしいらしいが...？

<details><summary>警告の内容</summary><div>

```
--> FOAM Warning : 
    From virtual Foam::Istream& Foam::ISstream::read(Foam::word&)
    in file db/IOstreams/Sstreams/ISstream.C at line 899
    Reading "0.orig/U" at line 18
    Missing 1 closing ')' while parsing

vector(

--> FOAM Warning :
    Reading "0.orig/U" at line 18
    Imbalanced '{' with ')'
--> FOAM Warning :
    Reading "0.orig/U" at line 18
    Too many closing '}' ... was a ';' forgotten?
--> FOAM Warning :
    Reading "0.orig/U" at line 39
    Too many closing '}' ... was a ';' forgotten?
--> FOAM Warning :
    Reading "0.orig/U" at line 43
    Imbalanced brackets
```

</div></details>

これはCursorに質問しても効果がなかったので，昔ながらのトライ＆エラー．多分スペースの存在が悪さをしていると思い詰めて書くと動いた．

```
Uinlet          #eval { vector(250*cos($theta),0,250*sin($theta)) };
```

### A.6 レイノルズ数Reの修正

NACA0012翼の既存データはRe=1,000,000までのものしかない(http://airfoiltools.com/airfoil/details?airfoil=n0012-il)．  

(ここのは6e+06: https://doc.openfoam.com/2312/examples/verification-validation/turbulent/naca0012/)

そこで流速 $U$ を調節することでReを揃えたい．

$$
Re := \frac{\rho U L}{\mu} \quad U = \frac{\mu Re}{\rho L}
$$

このケースは圧縮性流体であり，thermophysicalPropertiesを参照すると完全気体モデル，定数の粘性係数モデルを採用している．従って

$$
U = \frac{\mu Re}{\rho L} 
= \frac{\mu Re R^* T}{p L}
= \frac{\mu Re R T}{p L (10^{-3}m)}
= \frac{(1.82\times10^{-5}) \times 10^6 \times R \times 298}{10^5 \times 1 \times (10^{-3} \times 28.9)}
$$

となる．ここで $R^* / \mathrm{(J/(kg \cdot K))}$ は流体固有の気体定数， $R / \mathrm{(J/(mol \cdot K))}$ は一般気体定数， $m / \mathrm{(g/mol)}$ は流体のモル質量である．それぞれの量の値は，$R$ を除いてケースディレクトリの設定ファイルから読み取れる．

一般気体定数 $R$ は，OpenFOAMのソースから読み取れる．

<details><summary>openfoam/src/OpenFOAM/global/constants/physicoChemical/physicoChemicalConstants.C</summary><div>

```cpp
    dimensionedScalar
    (
        "R",
        physicoChemical::NA*physicoChemical::k
    ),
```

</div></details>

<details><summary>openfoam/src/OpenFOAM/global/constants/fundamental/fundamentalConstants.C</summary><div>

```cpp
    Foam::dimensionedScalar
    (
        "NA",
        dimensionSet(0, 0, 0, 0, -1), //Foam::dimless/Foam::dimMoles,
        6.0221417930e+23
    ),
```

</div></details>

<details><summary>openfoam/etc/controlDict</summary><div>

```
    SICoeffs
    {
        ...
        physicoChemical
        {
            mu mu [1 0 0 0 0 0 0] 1.66054e-27;
            k  k  [1 2 -2 -1 0 0 0] 1.38065e-23;
        }
        ...
    }
```

</div></details>

よって $R = 6.0221417930 \times 1.38065 = 8.31447$ であり，

$$
U 
= \frac{(1.82\times10^{-5}) \times 10^6 \times 8.31447 \times 298}{10^5 \times 1 \times (10^{-3} \times 28.9)}
= 15.60 \mathrm{m/s}
$$

と設定すれば $Re = 10^6$ となる．

### A.7 迎角に応じた揚抗比の算出

<details><summary>質問１</summary><div>

>ldratio.py
>```python
>import os
>import pandas as pd
>import math
>
>dir = os.path.dirname(__file__)
>
># データを読み込む際にコメント行とヘッダーを無視する
>df = pd.read_csv(f'{dir}/postProcessing/forces/0/force.dat', delim_whitespace=True, comment='#', header=None)
>
># 新しい列を追加する
>theta = math.radians(10)
>df['drag'] = math.sin(theta) * df[3] + math.cos(theta) * df[1]
>df['lift'] = math.cos(theta) * df[3] - math.sin(theta) * df[1]
>df['LDRatio'] = df['lift'] / df['drag']
>
># 結果を表示する
># print(df)
>
>ldratio = df['LDRatio'].iloc[-1]
># print(type(ldratio))
>print(ldratio)
>```
>このPythonスクリプトの中のmath.radians(10)の10の箇所を，任意の実行時引数で置き換えたい．また，引数は実数のみを受け付けるように例外処理もしたい．どのように修正すればよいか．


>```python
>import sys
>
># コマンドライン引数を取得
>try:
>    angle = float(sys.argv[1])  # 実数に変換
>    theta = math.radians(angle)  # 角度をラジアンに変換
>    # 以下略
>except IndexError:
>    print("引数が必要です")
>except ValueError:
>    print("実数を入力してください")
>```

</div></details>

的確な回答．bashやPythonのスクリプト作成においてCursorは非常に便利だ．

以上で，例えば
```
./Allrun-parallel 10
```
とすれば迎角10度の計算を実行し，そのときの揚抗比を標準出力するようにできた．続いて，Dakotaとインターフェースを接続して最適化が実行できるようにする．

### A.8 Dakota インターフェースの設定

公式ドキュメント["Coupling Dakota to a Simulation"](https://snl-dakota.github.io/docs/6.19.0/users/usingdakota/introduction/couplingtosimulations.html#couplingtosimulations-main)を参照する．

DakotaとOpenFOAMを結びつけるにはAnalysis Driverと呼ばれるものが必要．

<details><summary>以下，引用</summary><div>

>Dakota and the simulation model communicate by an exchange of parameters and responses. This is achieved by reading and writing short data files, which contain the parameter and response content.
>
>Dakota uses analysis drivers to handle the actual reading and writing of these short data files. Analysis drivers can be thought of as the “glue” between Dakota and the simulation model.
>
>An analysis driver could be:
>
>- An executable script that was manually authored by an analyst.
>
>- An executable file that was automatically generated by another tool (such as a Dakota GUI wizard, or a workflow in Next-Gen Workflow), based on existing knowledge about the black-box simulation model.

</div></details>

また，インストールしたDakotaのディレクトリにもドキュメントがあった：
- `<install-dir>/dakota-6.19.0-public-rhel8.Linux.x86_64-gui_cli/share/dakota/examples/official/drivers/bash/README.md`

<details><summary>以下，引用</summary><div>

>The driver has three main responsibilities.
>
>1. **Preprocessing**: Dakota provides parameter values to the driver by writing them to a *parameters file*.
>   It communicates the name of the parameters file to the driver by passing it as its first command line argument. 
>   In the preprocessing phase, the driver uses values it reads from the parameters file to construct valid
>   input for the simulation.
>2. **Execution**: The driver runs the user's simulation.
>3. **Postprocessing**: In this phase, the driver extracts response information from simulation output and
>   writes it to a correctly formatted Dakota results file. Dakota provides the
>   name of the results file as the second command line argument to the driver.

</div></details>

つまり，Dakotaの設定ファイル（今回のケースでは`dakota.in`）において

```
interface
  analysis_drivers = 'Allrun-parallel'
    fork
      work_directory named 'aerofoilNACA0012'
    parameters_file = 'params.in'
    results_file = 'results.out'
```

とするとき，実際にはOpenFOAMは

```sh
./aerofoilNACA0012/Allrun-parallel ./params.in ./results.out
```

と２つの引数付きで実行される（はず）．このフォーマットに合わせて実行ファイル（`Allrun-parallel`）を修正しないといけない．

#### *in, *.outの書式は？

となると，まずは`params.in`, `results.out`の書式がわからないことには`Allrun-parallel`を修正できない．下記Exampleを参考に作成する．

- https://github.com/snl-dakota/dakota-examples/blob/ca2ae351d50444ffe5549d9d4f7e0c96da168baa/training/exercises/input/driver.sh

`params.in`はある程度自分で定義してよく，`params.template`を用意して，`{変数名}`という記述を設けさえすればDakotaはそこを書き換えて`in`ファイルを生成するらしい．bashで読み込むので，下記のようにしてみた：

```python
# Input File Format:
# Each line represents a single parameter.
# White spaces within expressions are not allowed.
# Lines beginning with '#' or any text following '#' are considered comments.

theta={theta}   # angle of attack (deg)
```

`results.out`は１行ごとに`値` `変数名`の並びに書き出せば良いらしい．

```python
# Output File Format:
23.45 ldratio
```

#### Allrunの修正

`params.in` `results.out`の書式が（多分）わかったので，`Allrun-parallel`にAnalysis Driverの機能をもたせる修正を行う．

シェルスクリプトの作成に関してはChatGPTはとても便利だ：

<details><summary>質問２</summary><div>

>```sh
>theta=$(grep -Po '(?<=theta=)\d+' $1)  
>```
>この正規表現では，thetaに整数しか受け付けないようです．実数も受け付けるように修正してください．


>修正した正規表現は以下の通りです：
>```sh
>theta=$(grep -Po '(?<=theta=)[0-9]+([.][0-9]+)?' $1)
>```
>この修正により、"theta="の後に続く整数または実数にマッチするようになります。

</div></details>

<details><summary>質問３</summary><div>

>```sh
>theta=$(grep -Po '(?<=theta=)[0-9]+([.][0-9]+)?' $1)
>```
>この操作に，正規表現に該当するような文字列がファイル$1から得られなかった場合の例外処理を追加してください．


>以下のように例外処理を追加できます：
>```sh
>theta=$(grep -Po '(?<=theta=)[0-9]+([.][0-9]+)?' $1)
>if [ -z "$theta" ]; then
>    echo "Error: theta value not found in $1" &2
>    exit 1
>fi
>```
>この例外処理は、$1から得られた文字列が空の場合に、エラーメッセージを出力してスクリプトを終了します。

</div></details>

<details><summary>質問４</summary><div>

>今示してくれたスクリプトの意味を一行ずつ説明してください．


>```sh
>theta=$(grep -Po '(?<=theta=)[0-9]+([.][0-9]+)?' $1)
>```
>- この行は、ファイル$1内でtheta=の後に続く整数または実数を抽出し、変数thetaに格納します。
>```sh
>if [ -z "$theta" ]; then
>    echo "Error: theta value not found in $1" >&2
>    exit 1
>fi
>```
>- この行は、変数thetaが空である場合に、エラーメッセージを出力してスクリプトを終了します。

</div></details>

勉強になるなあ〜

受け付けられる実数のフォーマットを拡張したい：

<details><summary>質問５</summary><div>

>```sh
>theta=$(grep -Po '(?<=theta=)[0-9]+([.][0-9]+)?' $1)
>```
>ここでは，thetaに例えば12などの整数や12.34などの実数が入りますが，1.234e+03といった書式の実数をも受け付けたい場合，どのように修正すればよいですか．


>次のように修正することで、指数表記の実数も受け付けるようになります：
>```sh
>theta=$(grep -Po '(?<=theta=)[0-9]+([.][0-9]+)?([eE][+-]?[0-9]+)?' $1)
>```
>この修正により、"theta="の後に続く整数、実数、および指数表記の実数にマッチするようになります。

</div></details>

素晴らしい．表記ゆれへの配慮が行き届いている．`#`から始まるコメントの除外も実装してしまおう．

<details><summary>質問６</summary><div>

>”variable = 100# amount of machines”のような行でも，シェルスクリプトで"variable = 100"のみを読み取るにはどうしたらよいですか？


>シェルスクリプトで"variable = 100"のみを読み取るには、次のようにします：
>```sh
>grep -o '^[^#]*' filename
>```
>このコマンドは、ファイルから"#"以降の文字列を除外して出力します。

</div></details>

<details><summary>質問７</summary><div>

>上記コマンドがしていることを，できるだけ詳しく丁寧に教えてください．


>`grep -o '^[^#]*' filename`は、`filename`というファイルから行ごとにマッチするテキストを抽出するコマンドです。具体的には、次のことを行います：
>
>- `^`は行の先頭を表し、`#`以外の文字にマッチします。
>- `[^#]*`は`#`以外の文字が0回以上繰り返すことを表します。
>- `-o`オプションは、マッチした部分だけを出力します。
>
>つまり、このコマンドは`filename`から`#`で始まるコメント部分を除外し、`#`以降の文字列を無視して、コメントでない部分のみを抽出して出力します。

</div></details>

うーん，自力では思いつかなかった...

これでinterfaceは出来上がった．

### A.9 Dakota自身の最適化設定

インターフェースができたので，Dakotaそのもの設定に移る．適当にGitHubのExamplesを見て，こんなふうに書いてみた．

<details><summary>dakota.in</summary><div>

```
environment
  tabular_data
    tabular_data_file 'aerofoilNACA0012_optimization.dat'

method
  efficient_global
    seed = 20

variables
  continuous_design = 1
    descriptors   'theta'
    upper_bounds  20.0
    initial_point 10.0
    lower_bounds  0.0

interface
  fork
    analysis_drivers = 'sim-allrun-parallel'
    parameters_file = 'params.in'
    results_file = 'results.out'

responses
  id_responses = 'ldratio'
  objective_functions = 1
  sense "maximization"
  no_gradients
  no_hessians
```

</div></details>

これを書いた時点では項目の意味を理解しているわけではない．

[ここ](https://snl-dakota.github.io/docs/6.19.0/users/usingdakota/examples/gettingstarted.html)を参考にこんなコマンドで実行を試みる：
```sh
dakota-initialize # 自作
dakota -i dakota.in -o dakota.out > dakota.stdout 2> dakota.stderr 
```

とりあえず無事に動いた．

#### 最適化設定の調整

https://snl-dakota.github.io/docs/6.19.0/users/usingdakota/reference/method-efficient_global.html?highlight=efficient_global

### A.10 最適化結果の可視化

Pythonを用いて出力ファイルの内容を可視化する．

<details><summary>質問１</summary><div>

>aerofoilNACA0012_optimization.dat
>```
>%eval_id interface theta          ldratio        
>1        NO_ID      17.699249      4.411614712    
>2        NO_ID      19.43630057    3.744918651    
>3        NO_ID      8.83409923     20.38348785  
>...
>```
>このファイルを読み取って，横軸theta，縦軸ldratioの２Dプロットを作成するPythonスクリプトを作成してください．ただし，次の２通りのスクリプトを作成してください：
>1. matplotlibを用いてpngを出力．プロット範囲はデータ範囲から自動で決定する．
>2. plotlyを用いてhtmlを出力．プロット上の点にカーソルを合わせると，座標が表示されるように．

---

>```python
># matplotlibを使用してpngを出力
>import matplotlib.pyplot as plt
>
># データを読み込む
>data = """
>%eval_id interface theta          ldratio        
>1        NO_ID      17.699249      4.411614712    
>2        NO_ID      19.43630057    3.744918651    
>3        NO_ID      8.83409923     20.38348785    
>...
>"""
>
># データを整形
>data = data.strip().split('\n')
>data = [line.split() for line in data]
>data = [[float(val) for val in line[2:]] for line in data[1:]]
>
># プロット
>theta = [row[0] for row in data]
>ldratio = [row[1] for row in data]
>
>plt.plot(theta, ldratio, 'o')
>plt.xlabel('theta')
>plt.ylabel('ldratio')
>plt.title('2D Plot')
>plt.savefig('2d_plot.png')
>plt.show()
>```
>
>```python
># plotlyを使用してhtmlを出力
>import plotly.graph_objects as go
>
># データを整形
>theta = [row[2] for row in data]
>ldratio = [row[3] for row in data]
>
># プロット
>fig = go.Figure(data=go.Scatter(x=theta, y=ldratio, mode='markers'))
>fig.update_layout(title='2D Plot', xaxis_title='theta', yaxis_title='ldratio')
>fig.show()
>```

</div></details>

pandas使えばもっとコードがすっきりしそう．

<details><summary>質問２</summary><div>

>もう一度，２種類のスクリプトを作成してください．今回はデータの読み込みにpandasを使ってください．

---

>```python
># matplotlibを使用してpngを出力
>import pandas as pd
>import matplotlib.pyplot as plt
>
># データを読み込む
>df = pd.read_csv('aerofoilNACA0012_optimization.dat', delim_whitespace=True, skiprows=1)
>
># プロット
>plt.plot(df['theta'], df['ldratio'], 'o')
>plt.xlabel('theta')
>plt.ylabel('ldratio')
>plt.title('2D Plot')
>plt.savefig('2d_plot.png')
>plt.show()
>```
>
>```python
># plotlyを使用してhtmlを出力
>import pandas as pd
>import plotly.express as px
>
># データを読み込む
>df = pd.read_csv('aerofoilNACA0012_optimization.dat', delim_whitespace=True, skiprows=1)
>
># プロット
>fig = px.scatter(df, x='theta', y='ldratio', title='2D Plot', labels={'theta': 'Theta', 'ldratio': 'Ldratio'})
>fig.show()
>```

</div></details>

plotlyの方はエクスポートする操作がないので修正させる．

<details><summary>質問３</summary><div>

>２つめのスクリプトで，"fig"をエクスポートするように操作を追加してください．

---

>```python
># plotlyを使用してhtmlを出力
>import pandas as pd
>import plotly.express as px
>
># データを読み込む
>df = pd.read_csv('aerofoilNACA0012_optimization.dat', delim_whitespace=True, skiprows=1)
>
># プロット
>fig = px.scatter(df, x='theta', y='ldratio', title='2D Plot', labels={'theta': 'Theta', 'ldratio': 'Ldratio'})
>fig.write_html('2d_plot.html')
>fig.show()
>```

</div></details>


<details><summary>タイトル</summary><div>

</div></details>



