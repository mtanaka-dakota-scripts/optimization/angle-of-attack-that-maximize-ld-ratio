import os
import pandas as pd
import math
import sys

dir = os.path.dirname(__file__)

# コマンドライン引数を取得
try:
    angle = float(sys.argv[1])  # 実数に変換
    theta = math.radians(angle)  # 角度をラジアンに変換
    
    # データを読み込む際にコメント行とヘッダーを無視する
    df = pd.read_csv(f'{dir}/postProcessing/forces/0/force.dat', delim_whitespace=True, comment='#', header=None)

    # 新しい列を追加する
    df['drag'] = math.sin(theta) * df[3] + math.cos(theta) * df[1]
    df['lift'] = math.cos(theta) * df[3] - math.sin(theta) * df[1]
    df['LDRatio'] = df['lift'] / df['drag']

    # 結果を表示する
    # print(df)
    ldratio = df['LDRatio'].iloc[-1]
    print(f'{ldratio} ldratio') # with the format of Dakota results.out

except IndexError:
    print("引数が必要です")
except ValueError:
    print("実数を入力してください")

