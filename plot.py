import os
import pandas as pd
import matplotlib.pyplot as plt
import plotly.express as px

dir = os.path.dirname(__file__)

# フォルダが存在しない場合は作成
if not os.path.exists(f'{dir}/plot'):
    os.makedirs(f'{dir}/plot')

# データを読み込む
df = pd.read_csv(f'{dir}/aerofoilNACA0012_optimization.dat', delim_whitespace=True, header=0)

# matplotlibを使用してpngを出力
plt.plot(df['theta'], df['ldratio'], 'o')
plt.xlabel('theta')
plt.ylabel('ldratio')
plt.title('2D Plot')
plt.grid(True)  # 目盛線を追加
plt.savefig(f'{dir}/plot/2d_plot.png')
plt.show()

# plotlyを使用してhtmlを出力
fig = px.scatter(df, x='theta', y='ldratio', title='2D Plot', labels={'theta': 'Theta', 'ldratio': 'Ldratio'})
fig.write_html(f'{dir}/plot/2d_plot.html')
fig.show()
